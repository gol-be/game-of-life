﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace GameOfLife.Model
{
    public class Cell : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public Point UICoordinate { get; }
        public Point ArrayCoordinate { get; }
        public int PixelAmount { get; }
        public RelayCommand ClickButton { get; }
        public int GameWidth { get; }
        public int GameHeight { get; }
        public bool IsAlive { get; private set; }
        public bool WillBeAlive { get; private set; }
        public bool HasChanged => IsAlive != WillBeAlive;
        public IEnumerable<Point> Neighbours { get; }

        private Brush _Color;
        public Brush Color
        {
            get => _Color;
            set
            {
                if ( value != _Color )
                {
                    _Color = value;
                    RaisePropertyChanged( nameof( Color ) );
                }
            }
        }

        public Cell( Point coordinate, int gameWidth, int gameHeight, int pixelAmount, bool isAlive = false )
        {
            ArrayCoordinate = coordinate;
            GameWidth = gameWidth;
            GameHeight = gameHeight;
            IsAlive = isAlive;
            Neighbours = GetNeighboursCoordinates();
            PixelAmount = pixelAmount;
            UICoordinate = new( PixelAmount * ArrayCoordinate.X, PixelAmount * ArrayCoordinate.Y );
            Color = IsAlive ? Brushes.Green : Brushes.White;
            ClickButton = new( a => ToggleState(), a => true );
        }

        private void RaisePropertyChanged( string propertyName ) => PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        public void SetColor() => Color = IsAlive ? Brushes.Green : Brushes.White;

        public void GetNextState( GameBoard gameBoard )
        {
            var aliveNeighbours = Neighbours
                .Select( point => gameBoard[point] )
                .Count( cell => cell.IsAlive );

            if ( IsAlive )
                WillBeAlive = aliveNeighbours is >= 2 and <= 3;
            else
                WillBeAlive = aliveNeighbours == 3;
        }

        public void ApplyNextState()
        {
            if ( HasChanged )
                IsAlive = WillBeAlive;
            SetColor();
        }

        public void ToggleState()
        {
            IsAlive = !IsAlive;
            WillBeAlive = !IsAlive;
            SetColor();
        }

        private IEnumerable<Point> GetNeighboursCoordinates()
        {
            //we want all cells to have 8 neighbours.
            for ( int x = -1; x <= 1; x++ )
                for ( int y = -1; y <= 1; y++ )
                {
                    //skipping the initial cell which is at X = Y = 0
                    if ( x == 0 && y == 0 )
                        continue;

                    //tuple deconstruction
                    (int X, int Y) = (ArrayCoordinate.X + x, ArrayCoordinate.Y + y);

                    //this logic will let cells which are positioned on the edge(x == Width || x == 0 || y == Height || y == 0) to have neighbours on the opposide side
                    if ( X < 0 )
                        X += GameWidth;
                    else if ( X == GameWidth )
                        X = 0;
                    if ( Y < 0 )
                        Y += GameHeight;
                    else if ( Y == GameHeight )
                        Y = 0;

                    yield return new Point( X, Y );
                }
        }
    }
}
