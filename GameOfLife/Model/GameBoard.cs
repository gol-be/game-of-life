﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife.Model
{
    public class GameBoard
    {
        private readonly Cell[,] _Cells;
        public ObservableCollection<Cell> Cells { get; }
        public int Width { get; }
        public int Height { get; }
        public int PixelAmountPerCell { get; }

        public GameBoard( int width, int height, int pixelAmountPerCell )
        {
            Width = width;
            Height = height;
            PixelAmountPerCell = pixelAmountPerCell;
            _Cells = new Cell[Width, Height];
            InitializeCells();
            Cells = new ObservableCollection<Cell>( _Cells.OfType<Cell>() );
        }

        public Cell this[Point point] => _Cells[point.X, point.Y];
        public Cell this[int X, int Y] => _Cells[X, Y];

        private void InitializeCells()
        {
            for ( int x = 0; x < Width; x++ )
                for ( int y = 0; y < Height; y++ )
                    _Cells[x, y] = new Cell( new Point( x, y ), Width, Height, PixelAmountPerCell );
        }

        public void FillCellsRandom()
        {
            var random = new Random();

            for ( int x = 0; x < Width; x++ )
                for ( int y = 0; y < Height; y++ )
                    // 1/3 of cells should be alive
                    if ( random.Next( 0, 9 ) < 3 )
                        this[x, y].ToggleState();
        }

        public void ToggleCell( Point point ) => this[point].ToggleState();

        public void NextTurn()
        {
            GetNextGeneration();
            ApplyNextGeneration();
        }

        private void GetNextGeneration()
        {
            foreach ( var cell in _Cells )
                cell.GetNextState( this );
        }

        private void ApplyNextGeneration()
        {
            foreach ( var cell in _Cells )
                cell.ApplyNextState();
        }
    }
}
