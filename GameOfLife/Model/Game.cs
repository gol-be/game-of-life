﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GameOfLife.Model
{
    public class Game : INotifyPropertyChanged
    {
        public bool IsEditable => IsRunning == false;
        public RelayCommand CreateGameBoard { get; }
        public RelayCommand FillGameBoardRandomly { get; }
        public RelayCommand StartGame { get; }
        public RelayCommand StopGame { get; }
        private bool HasHeightWidthAndPixelAmount => Height != default && Width != default && PixelAmount >= 1;

        private bool _IsRunning = default;
        public bool IsRunning
        {
            get => _IsRunning;
            set
            {
                if ( value != _IsRunning )
                {
                    _IsRunning = value;
                    RaisePropertyChanged( nameof( IsEditable ) );
                }
            }
        }

        private int _Delay = 1000;
        public int Delay
        {
            get => _Delay;
            set
            {
                if ( value != _Delay )
                {
                    _Delay = value;
                    RaisePropertyChanged( nameof( Delay ) );
                }
            }
        }
        private GameBoard _GameBoard;
        public GameBoard GameBoard
        {
            get => _GameBoard;
            set
            {
                if ( value != _GameBoard )
                {
                    _GameBoard = value;
                    RaisePropertyChanged( nameof( GameBoard ) );
                }
            }
        }
        private int _PixelAmount = 10;
        public int PixelAmount
        {
            get => _PixelAmount;
            set
            {
                if ( value != _PixelAmount )
                {
                    _PixelAmount = value;
                    RaisePropertyChanged( nameof( PixelAmount ) );
                }
            }
        }

        private int _Width = 45;
        public int Width
        {
            get => _Width;
            set
            {
                if ( value != _Width )
                {
                    _Width = value;
                    RaisePropertyChanged( nameof( Width ) );
                }
            }
        }

        private int _Height = 40;
        public int Height
        {
            get => _Height;
            set
            {
                if ( value != _Height )
                {
                    _Height = value;
                    RaisePropertyChanged( nameof( Height ) );
                }
            }
        }

        public Game()
        {
            CreateGameBoard = new RelayCommand( a => GameBoard = new GameBoard( Width, Height, PixelAmount ), f => HasHeightWidthAndPixelAmount && !IsRunning );
            FillGameBoardRandomly = new RelayCommand( a => GameBoard.FillCellsRandom(), f => !IsRunning );
            StartGame = new RelayCommand( a => StartGameLoop(), f => !IsRunning );
            StopGame = new RelayCommand( a => StopGameLoop(), f => IsRunning );
        }


        public void StopGameLoop() => IsRunning = false;

        public void StartGameLoop()
        {
            IsRunning = true;
            ThreadPool.QueueUserWorkItem( o =>
            {
                while ( IsRunning )
                {
                    GameBoard.NextTurn();
                    Thread.Sleep( Delay );
                }
            } );
        }

        //INotifyPropertyChanged Implementation
        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged( string propertyName )
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

    }
}
