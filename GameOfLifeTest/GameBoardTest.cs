using NUnit.Framework;
using GameOfLife;
using GameOfLife.Model;
using System.Linq;

namespace GameOfLifeTest
{
    public class GameBoardTest
    {
        [Test]
        public void Constructor()
        {
            var width = 10;
            var height = 15;
            var pixelAmount = 5;
            var gameBoard = new GameBoard( width, height, pixelAmount );
            Assert.AreEqual( width, gameBoard.Width );
            Assert.AreEqual( height, gameBoard.Height );
            Assert.AreEqual( pixelAmount, gameBoard.PixelAmountPerCell );

            Assert.AreEqual( width * height, gameBoard.Cells.Count );
        }

        [Test]
        public void FillCellsRandom()
        {
            var width = 10;
            var height = 15;
            var pixelAmount = 5;
            var gameBoard = new GameBoard( width, height, pixelAmount );
            gameBoard.FillCellsRandom();
            var livingCells = gameBoard.Cells.Where( s => s.IsAlive ).Count();
            Assert.Greater( livingCells, 10 );
        }

        [Test]
        public void ToggleCell()
        {
            var width = 10;
            var height = 15;
            var pixelAmount = 5;
            var gameBoard = new GameBoard( width, height, pixelAmount );
            bool initialState = gameBoard[new( 1, 1 )].IsAlive;
            gameBoard.ToggleCell( new( 1, 1 ) );
            bool toggledState = gameBoard[new( 1, 1 )].IsAlive;
            Assert.AreEqual( initialState, toggledState );
        }

        [Test]
        public void NextTurn()
        {
            var width = 10;
            var height = 15;
            var pixelAmount = 5;
            GameBoard gameBoard = new GameBoard( width, height, pixelAmount );
            gameBoard.FillCellsRandom();
            gameBoard.NextTurn();
            GameBoard gameBoard2 = new GameBoard( width, height, pixelAmount );

            Assert.AreNotEqual( gameBoard, gameBoard2 );
        }
    }
}